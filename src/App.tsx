import * as React from 'react'
import * as ReactDOM from 'react-dom'

const Header = (): JSX.Element => {
  return <h1>Hello world!</h1>
}


ReactDOM.render(
  <Header />,
  document.getElementById('app') as HTMLElement
)


